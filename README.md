## Daily Scheduler App

This app creates a daily schedule's for task from 9:00 AM to 9:00PM
can handle up to two overlaping schedules


## Setting up App

Clone a copy of daily-scheduler-app from gitlab

```sh
git clone https://gitlab.com/sanjeeviraj/daily-scheduler-app.git
```
install the dependencies

```sh
npm install
```
Run the app

```sh
npm start
```

## Supported Browsers

Supports firefox,chrome browsers

## Schedule Tasks -mock DATA

Currently the schedules are pulled from src/scedule.json

## Ongoing improvements

- resolving warnings,linting
- using redux for state management
- A lot of refactor has to be done / smaller level components

