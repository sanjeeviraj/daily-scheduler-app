import React from 'react';
import './DailyScheduler.css';
import {buildScheduleBlock,displayTimer} from '../util';

class DailyScheduler extends React.Component{
    constructor(props){
        super();
        this.state ={list:false,taskObj:null};
    }
    componentDidMount() {
        
        let taskObj = buildScheduleBlock();
        //console.log(g);
        setTimeout(()=>{
            this.setState({list:true,taskObj:taskObj});
            //console.log(this.state.taskObj);
        },0);        
    }
    getTaskLayout() {
        let task =[];        
        let toggle =1;
        for(let i=1;i <= 24 ;i++) {
            /*let taskListClass;
            if(i%2 == 0){
                taskListClass =" taskSplit oddClass";
            } else {
                taskListClass =" taskSplit evenClass ";
            }*/
            let rowClass;
            //console.log('toggle',toggle);
            if(toggle == 1 ){                
                rowClass='rowTypeOne';
            } else if(toggle == 2) {                
                rowClass='rowTypeTwo';
            }else if(toggle == 3) {                
                rowClass='rowTypeThree';
            } else if(toggle == 4) {                
                rowClass='rowTypeFour';
                toggle = 0; //reset to 1
            }
            task.push(<div key={i.toString()} className={`taskSplit ${rowClass}`}></div>);
            toggle ++;           
        }
        return task;
    }
    getTime() {
        let time =[];
        let position = 0;
        let i =0
        for(let i in displayTimer) {
            let t_style;
            if(i%2 == 0){
               t_style = {
                    marginTop:(position-10)+'px',
                    position:'absolute',
                    right:'5px',
                    fontFamily: 'sans-serif',
                    fontWeight: 'bold',
                    fontSize: '1em'
                };
            } else{
                t_style = {
                    marginTop:(position-10)+'px',
                    position:'absolute',
                    right:'5px',
                    fontFamily: 'sans-serif',
                    fontWeight: 'bold',
                    fontSize: '0.7em'
                };
            }
            
            
            time.push(<div style={t_style}>{displayTimer[i]}</div>);
            position += 30;
            i++;
        } 
        return time;       
    }
    getTask() {
        let Obj = this.state.taskObj;
        let taskList =[];
        let i =0;
        Obj.map((key) => {            
            let taskPosition = {
                height:key.height,
                marginTop:key.position+'px'
            }                   
            taskList.push(
                <div key = {i} className={`taskdetail ${key.colClass}`} style={taskPosition}>
                {key.desc}
                </div>
            );
            i++;
        });
        return taskList;
    }
    render() {
        return(
            <div className="apparea">
                <div class="timearea">
                    {this.getTime()}                    
                </div>
                <div className="rowarea">                
                        {this.getTaskLayout()}                                            
                </div>
                <div className="taskarea">
                {this.state.list &&                    
                    this.getTask()                    
                }
                </div>                
            </div>
        );
    }
}

export default DailyScheduler;