import {schedule} from './scedule.json';

export const getTimeRange = (time,duration) => {
    let nTime = parseFloat(time);
    let nDuration = parseInt(duration);
    let finaltime;
    if(nDuration >= 60) {
        finaltime = nTime + (nDuration / 60);
    } else {
        finaltime = nTime + (nDuration /100);
    }
    return finaltime;
};

const timeMap = {
    '9':0,'10':60,'11':120,'12':180,'13':240,'14':300,
    '15':360,'16':420,'17':480,'18':540,'19':600,'20':660
};

export const buildScheduleBlock = () => {
    let tt = schedule;
    //console.log(tt);
    let finale = tt.map((task) => {
       return  blockBuilder(task);      
    });
  
    //get the overlapping timers
    let theEnd = findOverlaps(finale);
    console.log('theEnd',theEnd);
    return theEnd;
};

const blockBuilder = (task) => {
    let endTime = getTimeRange(task.time,task.duration);
    
    let nPosition;
    //let startTime = parseFloat(task.time);
    let givenStart = task.time;
    let startTime = parseFloat(givenStart.split(':').join('.')).toFixed(2);
   
   //console.log('overLaps',startTime);
    if (startTime % 1 != 0) {
        let ev = startTime.toString().split('.');       
        //console.log('timeMap[ev[0]]',timeMap[ev[0]]);
        nPosition = parseFloat(timeMap[ev[0]]) + parseFloat(ev[1]) ;
        //console.log('nPositiony',nPosition);
    } else {
        nPosition = timeMap[parseFloat(startTime)];
        //console.log('nPosition',nPosition);
    }
    let blockProperty  = {
        desc:task.description,
        height:task.duration+'px',
        position:nPosition,
        startTime:parseFloat(startTime),
        endTime:endTime,
        colClass:''
    }    
    return blockProperty;
};

const tilelineFilter = () => {
    let timeline =[];
    schedule.map((key) => {
        let time= key.time;
        let toNum = parseFloat(time.split(':').join('.')).toFixed(2);
        timeline.push(toNum);
    });
    return timeline;
};


const findOverlaps = (scheduledList) => {   
    for(let key of scheduledList) {
        
        let startTime = key.startTime;
        let endTime = key.endTime;
        
        let totalSlot = deepFind(scheduledList,startTime,endTime);
        if(totalSlot == 1){           
            key.colClass = 'col-3';           
        } else if(totalSlot == 2){
            key.colClass = 'col-2';            
        } else if(totalSlot == 3) {
            key.colClass = 'col-1';
           
        }
    }
    return scheduledList;
};
const deepFind = (sList,startTime,endTime) => {
    let  timeLines = extractTimeLines(sList);
    let slot = 0;
    for(let i in timeLines) {
       
        let rTime = timeLines[i].split('-');
        let chkStartTime = parseFloat(rTime[0]);
        let chkEndTime = parseFloat(rTime[1]);
        
        let flag = overlapFinder(startTime,endTime,chkStartTime,chkEndTime)
        if(flag == true) {
            slot = slot + 1;
        }
    }
    return slot;
}
const extractTimeLines = (data) => {
    let timelineArr = [];
    data.map((key) =>{
        let combine = key.startTime+'-'+key.endTime;
        timelineArr.push(combine);
    })
    return timelineArr;
}
const overlapFinder = (startTime,endTime,chkStartTime,chkEndTime) => {
  
    if(startTime >= chkStartTime && endTime <= chkEndTime)
    {	
        return true;
    }
    if(chkStartTime >= startTime && chkEndTime <= endTime)
    {	
        return true;
    }    
    return false;
}

export const displayTimer = ['9:00 AM','9:30','10:00 AM','10:30','11:00 AM','11:30','12:00 PM','12:30','1:00 PM','1:30','2:00 PM','2:30',
'3:00 PM','3:30','4:00 PM','4:30','5:00 PM','5:30','6:00 PM','6:30','7:00 PM','7:30','8:00 PM','8:30','9:00 PM'];